## Instalação

- Run composer install
- Editar .env e setar suas configurações do banco de dados
- Run php artisan key:generate
- Run php artisan migrate

## Projeto

- Crud com todas as funcionalidades
-- UPDATE
-- CREATE
-- READ
-- DELETE

- Responsivo
- Utilização da API via cep

## Database

- Postgres

## Famework

- Laravel

 ## Framework de estilização

 - Bootstrap

## linguagem

- PHP 7
